package com.automation.practice.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultPage {

    WebDriver driver;

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectProductForName(String nomeDoProduto) {
        driver.findElement(By.xpath(".//*[contains(@id,'center_column')]//*[contains(@class, 'product-container')]//*[contains(@title, '".concat(nomeDoProduto).concat("')]//*[contains(@itemprop, 'image')]"))).click();
    }
}
